-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Мар 24 2021 г., 20:54
-- Версия сервера: 8.0.23-0ubuntu0.20.10.1
-- Версия PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60581bak`
--

-- --------------------------------------------------------

--
-- Структура таблицы `binding`
--

CREATE TABLE `binding` (
  `ID` int NOT NULL COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Наименование переплета'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `binding`
--

INSERT INTO `binding` (`ID`, `name`) VALUES
(1, 'Пружина'),
(2, 'Скоба'),
(3, 'Переплет (книжный)'),
(4, 'Японский переплет');

-- --------------------------------------------------------

--
-- Структура таблицы `buyer`
--

CREATE TABLE `buyer` (
  `id` int NOT NULL COMMENT 'ID',
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Имя',
  `last_name` varchar(255) NOT NULL COMMENT 'Фамилия',
  `address` varchar(255) DEFAULT NULL COMMENT 'Адрес',
  `zip_code` int DEFAULT NULL COMMENT 'Индекс',
  `BD` date DEFAULT NULL COMMENT 'День рождения',
  `phone` varchar(12) DEFAULT NULL COMMENT 'Номер телефона',
  `e-mail_login` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Логин (Почта)',
  `password` varchar(255) NOT NULL COMMENT 'Пароль'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `buyer`
--

INSERT INTO `buyer` (`id`, `first_name`, `last_name`, `address`, `zip_code`, `BD`, `phone`, `e-mail_login`, `password`) VALUES
(1, 'Александр', 'Смирнов', 'Москва, проспект Ленина 69, кв. 17', 657000, NULL, '8904878833', 'SmiRnov343@mail.ru', 'ttrrrtrtrt'),
(2, 'Аделина', 'Александрова', 'г. Сургут, ул. Лермонтова 16, кв. 7', NULL, '1999-12-16', NULL, 'AlexAlex@gmail.com', 'dori45645'),
(3, 'Иван', 'Иванов', NULL, NULL, NULL, NULL, 'Iva001@yandex.ru', '12345'),
(4, 'Василий', 'Пермяков', NULL, NULL, '2001-03-17', '+79973296916', 'sausage@gmail.com', 'vasya123'),
(9, 'Иван', 'Васильевич', 'г. Меняет, ул. Профессию 23, кв. 345', 651516, '1897-12-22', '+76899854554', 'sovetskoeKino@mail.ru', 'StalinNavsegda'),
(10, 'Тиса', 'Абиссинская', 'п. Кошачий, ул. Ленина 69, 16', NULL, '1897-08-15', NULL, 'MewMew@yandex.ru', 'KitiKat');

-- --------------------------------------------------------

--
-- Структура таблицы `constract`
--

CREATE TABLE `constract` (
  `id` int NOT NULL COMMENT 'ID',
  `id_order` int DEFAULT NULL COMMENT 'Id заказа',
  `id_binding` int NOT NULL COMMENT 'тип переплета',
  `id_format` int NOT NULL COMMENT 'Формат',
  `orientation` enum('Книжная','Альбомная') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ориентация',
  `id_paper` int NOT NULL COMMENT 'id бумаги',
  `numder_of_sheets` int NOT NULL COMMENT 'Кол-во листов',
  `cost` double NOT NULL COMMENT 'стоимость'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Конструктор блокнотов';

--
-- Дамп данных таблицы `constract`
--

INSERT INTO `constract` (`id`, `id_order`, `id_binding`, `id_format`, `orientation`, `id_paper`, `numder_of_sheets`, `cost`) VALUES
(1, 6, 1, 3, NULL, 5, 30, 765),
(2, 5, 4, 1, 'Альбомная', 6, 46, 545);

-- --------------------------------------------------------

--
-- Структура таблицы `format`
--

CREATE TABLE `format` (
  `ID` int NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `format`
--

INSERT INTO `format` (`ID`, `name`) VALUES
(1, 'A4'),
(2, 'A5'),
(3, '21x21'),
(4, 'B5');

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE `order` (
  `id` int NOT NULL COMMENT 'ID',
  `id_buyer` int NOT NULL COMMENT 'ID  покупателя',
  `date_order` datetime NOT NULL COMMENT 'Дата и время заказа',
  `status_order` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Дата отправки',
  `cost` double NOT NULL COMMENT 'Стоимость заказа',
  `delivery` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Способ доставки',
  `payment` enum('Оплачено','Ожидает оплаты') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Статус оплаты'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `id_buyer`, `date_order`, `status_order`, `cost`, `delivery`, `payment`) VALUES
(1, 1, '2021-03-10 19:23:12', 'Передан в службу доставки', 3000, 'Почта России, оплата через службу  доставки', 'Ожидает оплаты'),
(2, 1, '2021-01-12 19:23:12', 'Клиент получил товар', 1200, 'Почта России', 'Оплачено'),
(3, 2, '2021-03-23 06:26:02', 'Собираем', 300, 'CDEK', 'Ожидает оплаты'),
(4, 2, '2021-03-19 17:50:59', 'Передаем в службу доставки', 600, 'CDEK', 'Оплачено'),
(5, 10, '2021-03-24 14:44:43', 'Собираем', 545, 'DPD', 'Оплачено'),
(6, 9, '2020-11-10 20:22:43', 'Получен', 1965, 'Почта России', 'Оплачено');

-- --------------------------------------------------------

--
-- Структура таблицы `OrderProduct`
--

CREATE TABLE `OrderProduct` (
  `id_order` int NOT NULL COMMENT 'ID_покупателя',
  `id_product` int NOT NULL COMMENT 'ID_товара'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `OrderProduct`
--

INSERT INTO `OrderProduct` (`id_order`, `id_product`) VALUES
(1, 3),
(1, 3),
(1, 2),
(2, 2),
(2, 2),
(3, 1),
(4, 2),
(6, 1),
(6, 1),
(6, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `paper`
--

CREATE TABLE `paper` (
  `id` int NOT NULL COMMENT 'ID',
  `target` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'назначение бумаги',
  `facture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'фактура бумаги',
  `material` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'состав  бумагми',
  `density` int DEFAULT NULL COMMENT 'Плотность бумаги',
  `company` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Производитель',
  `price` double DEFAULT NULL COMMENT 'Цена за лист А4'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `paper`
--

INSERT INTO `paper` (`id`, `target`, `facture`, `material`, `density`, `company`, `price`) VALUES
(1, 'Для принтера (офисная)', 'гладкая', 'целлюлоза 100%', 80, 'SvetoCopy', 0.49),
(2, 'Для акварели', 'Grain Fin (Фин)', 'Хлопок 100%', 300, 'Малевичъ', 56.6),
(3, 'Для маркеров', NULL, NULL, 75, 'Palazzo', 9.225),
(4, 'Для маркеров', 'Односторонняя, глянцевая', NULL, 130, 'Сонет', 6.25),
(5, NULL, 'Односторонняя, матовая', NULL, NULL, 'Copic', NULL),
(6, 'Для графики', NULL, NULL, NULL, 'IQ', NULL),
(7, 'Для графики', 'Гладкая, матовая', 'Целлюлоза 100%', 160, 'IQ', 1.26);

-- --------------------------------------------------------

--
-- Структура таблицы `Product`
--

CREATE TABLE `Product` (
  `id` int NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Наиминование товара',
  `id_category` int NOT NULL COMMENT 'ID категории',
  `id_paper` int NOT NULL COMMENT 'Тип бумаги',
  `id_binding` int NOT NULL COMMENT 'ID переплета',
  `number_of_sheets` int NOT NULL COMMENT 'Кол-во листов',
  `price` double DEFAULT NULL COMMENT 'Цена',
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Изображение товара',
  `characteristics` text COMMENT 'Характеристика товара'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Product`
--

INSERT INTO `Product` (`id`, `name`, `id_category`, `id_paper`, `id_binding`, `number_of_sheets`, `price`, `photo`, `characteristics`) VALUES
(1, 'Тетрадь-блокнот с мягкой обложкой', 1, 1, 2, 20, 300, NULL, 'Эта тетрадь такая маленькая! Возьми ее с собой куда угодно, чтобы всегда было, куда было записать свои идеи.'),
(2, 'Синий блокнот', 1, 6, 1, 40, 600, '*ссылка на фото*', 'Этот блокнот с синей обложкой напомнит вам о небе                                                                                                          '),
(3, 'Скетчбук зеленый', 2, 2, 3, 64, 1200, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `Product_category`
--

CREATE TABLE `Product_category` (
  `id` int NOT NULL COMMENT 'ID ',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Наиминование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Product_category`
--

INSERT INTO `Product_category` (`id`, `name`) VALUES
(1, 'Блокнот'),
(2, 'Скетчбук'),
(3, 'Палетка для красок');

-- --------------------------------------------------------

--
-- Структура таблицы `Raiting`
--

CREATE TABLE `Raiting` (
  `id_product` int NOT NULL COMMENT 'ID товара',
  `id_buyer` int NOT NULL COMMENT 'ID  покупателя',
  `raview` text COMMENT 'отзыв',
  `raiting` int NOT NULL COMMENT 'оценка'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Raiting`
--

INSERT INTO `Raiting` (`id_product`, `id_buyer`, `raview`, `raiting`) VALUES
(2, 2, 'Мне все понравилось!', 5),
(1, 2, NULL, 4),
(1, 9, 'Очень не понравилась бумага, в остальном нормально', 3),
(2, 9, 'Прекрасный блокнот! Никак не нарадуюсь', 5);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `binding`
--
ALTER TABLE `binding`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `buyer`
--
ALTER TABLE `buyer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `e-mail_login` (`e-mail_login`);

--
-- Индексы таблицы `constract`
--
ALTER TABLE `constract`
  ADD PRIMARY KEY (`id`),
  ADD KEY `constract_ibfk_2` (`id_format`),
  ADD KEY `id_paper` (`id_paper`),
  ADD KEY `id_binding` (`id_binding`),
  ADD KEY `id_order` (`id_order`);

--
-- Индексы таблицы `format`
--
ALTER TABLE `format`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_byer` (`id_buyer`),
  ADD KEY `id_payment` (`payment`);

--
-- Индексы таблицы `OrderProduct`
--
ALTER TABLE `OrderProduct`
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_order` (`id_order`);

--
-- Индексы таблицы `paper`
--
ALTER TABLE `paper`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `Product_ibfk_2` (`id_binding`),
  ADD KEY `id_paper` (`id_paper`),
  ADD KEY `id_category_2` (`id_category`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `Product_category`
--
ALTER TABLE `Product_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Raiting`
--
ALTER TABLE `Raiting`
  ADD KEY `id_buyer` (`id_buyer`),
  ADD KEY `id_product` (`id_product`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `binding`
--
ALTER TABLE `binding`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `buyer`
--
ALTER TABLE `buyer`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `constract`
--
ALTER TABLE `constract`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `format`
--
ALTER TABLE `format`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `paper`
--
ALTER TABLE `paper`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `Product`
--
ALTER TABLE `Product`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `Product_category`
--
ALTER TABLE `Product_category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID ', AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `constract`
--
ALTER TABLE `constract`
  ADD CONSTRAINT `constract_ibfk_2` FOREIGN KEY (`id_format`) REFERENCES `format` (`ID`) ON DELETE RESTRICT,
  ADD CONSTRAINT `constract_ibfk_3` FOREIGN KEY (`id_paper`) REFERENCES `paper` (`id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `constract_ibfk_4` FOREIGN KEY (`id_binding`) REFERENCES `binding` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `constract_ibfk_5` FOREIGN KEY (`id_order`) REFERENCES `order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`id_buyer`) REFERENCES `buyer` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `OrderProduct`
--
ALTER TABLE `OrderProduct`
  ADD CONSTRAINT `OrderProduct_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `Product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `OrderProduct_ibfk_2` FOREIGN KEY (`id_order`) REFERENCES `order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Product`
--
ALTER TABLE `Product`
  ADD CONSTRAINT `Product_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `Product_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Product_ibfk_2` FOREIGN KEY (`id_binding`) REFERENCES `binding` (`ID`) ON DELETE RESTRICT,
  ADD CONSTRAINT `Product_ibfk_3` FOREIGN KEY (`id_paper`) REFERENCES `paper` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Raiting`
--
ALTER TABLE `Raiting`
  ADD CONSTRAINT `Raiting_ibfk_1` FOREIGN KEY (`id_buyer`) REFERENCES `buyer` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Raiting_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `Product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
