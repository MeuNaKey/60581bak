<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($buyer)): ?>
            <div class="media mt-5 mb-5">
                <img style="width: 128px" src="https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg" class="align-self-start mr-3" alt="...">
                <div class="media-body">
                    <h5 class="mt-0"><?php echo($buyer['first_name'].' '.$buyer['last_name']); ?> (<?= esc($buyer['e-mail_login'])?>)</h5>
                    <p><b>Номер телефона:</b> <?php if (!empty($buyer['phone'])) echo($buyer['phone']); else echo('Не указан'); ?></p>
                    <p><b>Адрес:</b> <?php if (!empty($buyer['address'])) echo($buyer['address']); else echo('Не указан'); ?> </p>
                    <p><b>Индекс:</b> <?php if (!empty($buyer['zip_code'])) echo($buyer['zip_code']); else echo('Не указан'); ?></p>
                    <p><b>Дата рождения:</b> <?php if (!empty($buyer['BD'])) echo($buyer['BD']); else echo('Не указан'); ?></p>
                </div>
            </div>
        <?php else : ?>
            <p><b>Что-то пошло не так.</b> Не удалось найти данные о пользователе.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>