<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <h2 class="mt-3">Данные</h2>

    <?php if (!empty($buyer) && is_array($buyer)): ?>

        <?php foreach ($buyer as $item): ?>

            <div class="card mb-3" style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img style="max-width: 16rem" src="https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg" class="card-img" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo($item['first_name'].' '.$item['last_name']); ?></h5>
                            <p class="card-text">Логин: <?= $item['e-mail_login']; ?></p>
                            <a href="<?= base_url()?>/index.php/buyer/view/<?= esc($item['id']); ?>" class="btn btn-dark">Подробнее...</a>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    <?php else : ?>
        <p>Невозможно найти данные о пользователях.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>

