<?php namespace App\Controllers;

use App\Models\BuyerModel;

class Buyer extends BaseController

{
    public function index() //Обображение всех записей
    {
        $model = new BuyerModel();
        $data ['buyer'] = $model->getBuyer();
        echo view('buyer/view_all', $data);
    }

    public function view($id = null) //отображение одной записи
    {
        $model = new BuyerModel();
        $data ['buyer'] = $model->getBuyer($id);
        echo view('buyer/view', $data);
    }

}